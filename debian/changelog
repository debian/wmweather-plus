wmweather+ (2.19~alpha+ds-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * d/patches: add patch to fix FTBFS (Closes: #1067547)

 -- Jeremy Sowden <azazel@debian.org>  Thu, 18 Apr 2024 21:40:43 +0100

wmweather+ (2.19~alpha+ds-1) unstable; urgency=medium

  * Bump standards version to 4.6.1.
  * Bump debhelper version to 13, drop d/compat.
  * Update maintainer address.
  * d/control:
    - update libpcre3-dev to libpcre2-dev. (Closes: #999928)

 -- Gürkan Myczko <tar@debian.org>  Tue, 06 Sep 2022 23:02:18 +0200

wmweather+ (2.18-1) unstable; urgency=medium

  * New upstream version.
  * Bump standards version to 4.5.0.
  * Update copyright years.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Sun, 26 Apr 2020 14:47:59 +0200

wmweather+ (2.17-2) unstable; urgency=medium

  * Add Vcs fields.
  * Bump standards version to 4.4.1.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Mon, 25 Nov 2019 16:07:51 +0100

wmweather+ (2.17-1) unstable; urgency=medium

  * New upstream version. (Closes: #893609, #896939)
  * Bump debhelper version to 11.
  * Bump standards version to 4.1.4.
  * debian/menu: removed.
  * debian/rules: simplyfied.
  * Add myself to uploaders.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Thu, 26 Apr 2018 11:10:27 +0200

wmweather+ (2.15-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * debian/control:
    - Switch libwraster3-dev in Build-Depends to libwraster-dev
      (Closes: #784702).

 -- Doug Torrance <dtorrance@monmouthcollege.edu>  Tue, 19 May 2015 18:55:55 -0500

wmweather+ (2.15-1) unstable; urgency=medium

  * New upstream release
    - Data download works again (Closes: #767523)
  * debian/rules:
    - Honor dpkg-buildflags to include hardening
  * Update debian/copyright with proper Format: line
  * Remove lintian override unknown-copyright-format-uri
  * debian/control:
    - Update Vcs- fields to current links for Alioth
    - Standards-Version 3.9.6 -- no changes required
    - Remove Martin F. Krafft from Uploaders (Closes: #719092)

 -- Martin Stigge <martin@stigge.org>  Fri, 31 Oct 2014 16:50:50 +0100

wmweather+ (2.13-1) unstable; urgency=low

  * New upstream release
    - Fix FTBFS with binutils-gold (Closes: #556684)
    - Fix potential crash with libcurl (Closes: #593390)
  * Change to new source format 3.0 (quilt)
  * Standards-Version 3.9.2 -- no changes required
  * Lintian overrides:
    - Remove override for manpage error, fixed upstream
    - Add override for DEP-5 format URL
  * debian/rules:
    - Simplify heavily, now just use mostly plain dh calls
  * debian/control:
    - Remove trailing empty paragraph in description. Thanks, lintian!
    - Add VCS info
    - Bump debhelper build dependency (7.0.50 for override targets)
  * debian/copyright:
    - Change to machine readable format (DEP-5)
    - Mention copyrights of other contributors

 -- Martin Stigge <martin@stigge.org>  Sun, 25 Dec 2011 00:24:48 +0100

wmweather+ (2.11-4) unstable; urgency=low

  * Change dependency from libcurl to libcurl4-gnutls (Closes: #532823)
  * Change debhelper compatibility from 4 to 7
    - dh_prep instead of dh_clean -k 
    - Make build-dependency for debhelper versioned
  * Standards-Version to 3.8.3 -- no changes required
  * Add lintian-override for manpage "cannot adjust line" error

 -- Martin Stigge <martin@stigge.org>  Fri, 30 Oct 2009 23:12:48 +0100

wmweather+ (2.11-3) unstable; urgency=low

  * debian/control:
    * Moved "Homepage:" to Source package (Thanks, Anibal!)
    * Added ${misc:Depends} to "Depends:" (Thanks, Anibal!)

 -- Martin Stigge <martin@stigge.org>  Wed, 26 Mar 2008 11:21:58 +0100

wmweather+ (2.11-2) unstable; urgency=low

  * Add -Wl,-as-needed to LDFLAGS to get rid of unnecessary lib dependencies

 -- Martin Stigge <martin@stigge.org>  Tue, 18 Mar 2008 15:15:03 +0100

wmweather+ (2.11-1) unstable; urgency=low

  * New upstream release
    - Migrated from libwww to libcurl (Closes: #459214)
    - displaydelay patch incorporated
    - snprintf3 issue also solved
  * dpatch removed since no patches needed anymore
  * Standards-Version 3.7.3
    - Menu section updated
  * debian/rules: Fix debian-rules-ignores-make-clean-error. Thanks, lintian!
  * debian/control: Make Homepage a regular Pseudo-header. Thanks, lintian!
  * Watch file added

 -- Martin Stigge <martin@stigge.org>  Sun, 03 Feb 2008 21:51:37 +0100

wmweather+ (2.9-4) unstable; urgency=low

  * dpatch is now used for patches (added also to Build-Depends)
  * Applied snprintf3 patch from BTS to fix FTBFS (Closes: #419644)

 -- Martin Stigge <martin@stigge.org>  Sun, 29 Apr 2007 22:19:54 +0200

wmweather+ (2.9-3) unstable; urgency=low

  * Applied displaydelay patch from BTS (Closes: #416713)
  * Rebuild with new debhelper moves menu file finally to /usr/share/menu
  * New FSF address in copyright file
  * Standards-Version 3.7.2 -- no changes required.
  * Adapted debian/control to include Homepage information properly

 -- Martin Stigge <martin@stigge.org>  Mon, 16 Apr 2007 19:34:12 +0200

wmweather+ (2.9-2) unstable; urgency=low

  * New build-dependency on libwraster3-dev, replacing
    libwraster2-dev (Closes: #282288)
  * Added Martin F. Krafft to Uploaders

 -- Martin Stigge <martin@stigge.org>  Mon, 22 Nov 2004 13:55:48 +0100

wmweather+ (2.9-1) unstable; urgency=low

  * New upstream release (Closes: #263098)
  * Added quotes to strings in debian/menu

 -- Martin Stigge <martin@stigge.org>  Sat,  7 Aug 2004 22:34:27 +0200

wmweather+ (2.5-2) unstable; urgency=low

  * Fixed avn-/eta-/mrf-uri in manpage. (Closes: #229238)

 -- Martin Stigge <martin@stigge.org>  Sat, 24 Jan 2004 00:32:54 +0100

wmweather+ (2.5-1) unstable; urgency=low

  * New maintainer. (Closes: #200111)
  * New upstream release. (Closes: #225250)
    - Multiple instances now possible. (Closes: #200110)
    - Added "forget-warning-zones" option to override system default for
    warning zones. (Closes: #185651)
  * Applied atexit() patch (taken from CVS)
  * Standards-Version 3.6.1 -- no changes required.

 -- Martin Stigge <martin@stigge.org>  Fri,  2 Jan 2004 18:03:33 +0100

wmweather+ (2.4-2) unstable; urgency=low

  * Add an empty binary-indep target to comply with policy.
  * Clean up debian/rules.
  * Standards-Version 3.6.0 -- no changes required.

 -- Joshua Kwan <joshk@triplehelix.org>  Sun, 17 Aug 2003 02:03:33 -0700

wmweather+ (2.4-1) unstable; urgency=low

  * New maintainer. (Closes: #186093)
  * New upstream release.
  * Converted debian/rules to use debhelper.
  * The previously Debian-specific fixes (#184306, #181245) are now
    included upstream, just to note.

 -- Joshua Kwan <joshk@triplehelix.org>  Sun, 20 Apr 2003 00:04:08 -0700

wmweather+ (2.3-5) unstable; urgency=low

  * Fixed manual page, closes: #184306.
  * Updated standards version.

 -- Martin A. Godisch <godisch@debian.org>  Tue, 11 Mar 2003 16:31:41 +0100

wmweather+ (2.3-4) unstable; urgency=low

  * Fixed postinst and postrm.
  * Updated maintainer email address.

 -- Martin A. Godisch <godisch@debian.org>  Thu, 27 Feb 2003 16:45:51 +0100

wmweather+ (2.3-3) unstable; urgency=low

  * Fixed some more unsigned chars.

 -- Martin A. Godisch <godisch@tcs.inf.tu-dresden.de>  Mon, 17 Feb 2003 14:25:10 +0100

wmweather+ (2.3-2) unstable; urgency=low

  * Fixed char signedness and other compiler warnings, closes: #181245.
  * Removed build-dependency on debhelper.

 -- Martin A. Godisch <godisch@tcs.inf.tu-dresden.de>  Sun, 16 Feb 2003 23:16:06 +0100

wmweather+ (2.3-1) unstable; urgency=low

  * New upstream release.
  * Updated debian/control.

 -- Martin A. Godisch <godisch@tcs.inf.tu-dresden.de>  Wed, 29 Jan 2003 00:31:50 +0100

wmweather+ (2.1-1) unstable; urgency=low

  * Initial release, closes: #177266.

 -- Martin A. Godisch <godisch@tcs.inf.tu-dresden.de>  Tue, 21 Jan 2003 22:55:36 +0100
